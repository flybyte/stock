package com.control.back.halo.basic.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.control.back.halo.basic.commons.Constants;
import com.control.back.halo.basic.entity.User;

public class UserUtils {

    public static String getCurrentUserName() {
        return getCurrentUser().getUsername();
    }

    public static User getCurrentUser() {
        if(RequestContextHolder.getRequestAttributes() != null){
        
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            HttpSession session = request.getSession();
            return (User) session.getAttribute(Constants.USER_SESSION_ID);
        }
        return new User();
    }

}
