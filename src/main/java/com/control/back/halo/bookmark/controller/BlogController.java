package com.control.back.halo.bookmark.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.control.back.halo.basic.controller.BaseController;
import com.control.back.halo.basic.log4j.Logger;
import com.control.back.halo.bookmark.entity.Article;
import com.control.back.halo.bookmark.service.IBlogService;

@Controller
@RequestMapping(value = "/blog")
public class BlogController extends BaseController {

    private static Logger logger = Logger.getLogger(BlogController.class);

    @Autowired
    IBlogService          blogService;

    @RequestMapping(value = { "", "/index" }, method = RequestMethod.GET)
    public String index(Model model) throws ClassNotFoundException, IOException {
        return "/blog/index";
    }

    @RequestMapping(value = "/list")
    public @ResponseBody Page<Article> list(Pageable pageable) {
        Page<Article> page = blogService.findAll(pageable);
        return page;
    }

    @RequestMapping("/readMark")
    public String readMark(String url, Model model) {
        try {
            model.addAttribute("article", blogService.getArticle(url));
        } catch (Exception e) {
            logger.error("get article error!", e);
        }
        return "/blog/article";
    }

    @RequestMapping("/readMarkView")
    public String readMarkView(String url, Model model) {
        model.addAttribute("articleUrl", url);
        return "/category/readMarkView";
    }
}
