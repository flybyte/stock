package com.control.back.halo.bookmark.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.control.back.halo.bookmark.entity.Article;
import com.control.back.halo.basic.dao.IBaseDao;

@Repository(value = "blogDaoImpl")
public interface IBlogDao extends IBaseDao<Article, Long> {

	@Query("from Article a where a.urlHashCode = ?1")
	public Article getByUrlHashCode(Integer urlHashCode);

}
