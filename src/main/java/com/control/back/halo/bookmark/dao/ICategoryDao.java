package com.control.back.halo.bookmark.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.control.back.halo.basic.dao.IBaseDao;
import com.control.back.halo.basic.entity.User;
import com.control.back.halo.bookmark.entity.Category;

@Repository(value = "categoryDaoImpl")
public interface ICategoryDao extends IBaseDao<Category, Long> {

    @Query("from Category c where c.createdBy = ?1")
    public List<Category> findAllByAuth(User user);

    @Query(nativeQuery = true, value = "SELECT * FROM halo_category c where created_by_id = ?1 and title_hash_code in ( SELECT title_hash_code FROM halo_category where created_by_id = ?1 group by title_hash_code having count(1) > 1)  order by title_hash_code")
    public List<Category> findRepeatCategory(Long userId);

    @Query("from Category u where u.parent.id in ?1")
    public List<Category> findByParentIds(List<Long> parentIds);

    @Query("from Category u where u.id in ?1")
    public List<Category> findByIds(List<Long> ids);

    @Query("from Category c where c.title like ?1")
    public List<Category> findCategoryByName(String name);
}
