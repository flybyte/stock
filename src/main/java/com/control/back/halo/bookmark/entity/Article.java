package com.control.back.halo.bookmark.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.control.back.halo.basic.entity.BaseEntity;

@Entity
@Table(name = "halo_article")
public class Article extends BaseEntity {

    /**
     */
    private static final long serialVersionUID = -4139363474142293483L;

    /**
     * 标题
     */
    private String            title;

    /**
     * 图片地址
     */
    private String            imagePath;

    /**
     * 内容
     */
    private String            content;

    /**
     * 文本内容
     */
    private String            textContent;

    /**
     * 摘要
     */
    private String            digest;

    /**
     * 标签
     */
    private String            contentWithTags;

    /**
     * 文章发布时间
     */
    private Date              publishDate;

    /**
     * 转载连接
     */
    private String            url;

    /**
     * url的hashCode
     */
    private int               urlHashCode;

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public int getUrlHashCode() {
        return urlHashCode;
    }

    public void setUrlHashCode(int urlHashCode) {
        this.urlHashCode = urlHashCode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getContentWithTags() {
        return contentWithTags;
    }

    public void setContentWithTags(String contentWithTags) {
        this.contentWithTags = contentWithTags;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }
}
