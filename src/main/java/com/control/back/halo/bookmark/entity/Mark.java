package com.control.back.halo.bookmark.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.annotation.JSONField;
import com.control.back.halo.basic.entity.BaseEntity;

@Entity
@Table(name = "halo_mark")
public class Mark extends BaseEntity {

    /**
     */
    private static final long serialVersionUID = -6878822625739519934L;

    /**
     * name,cateid,add_date,last_modifed,href,icon
     */
    @Column(length = 512)
    String name;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    Category category;
    String addDate;
    String lastModifed;
    @Column(length = 1024)
    String href;
    @Column(length = 1024)
    String icon;

    /**
     * 是否有效
     */
    Boolean isValid;
    Integer hrefHashCode;
    
    public Integer getHrefHashCode() {
        return hrefHashCode;
    }

    public void setHrefHashCode(Integer hrefHashCode) {
        this.hrefHashCode = hrefHashCode;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JSONField(serialize = false)
    public Category getCategory() {
        return category;
    }

    public String getCategoryName() {
        return category.getTitle();
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getAddDate() {
        return addDate;
    }

    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }

    public String getLastModifed() {
        return lastModifed;
    }

    public void setLastModifed(String lastModifed) {
        this.lastModifed = lastModifed;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((category == null) ? 0 : category.hashCode());
        result = prime * result + ((href == null) ? 0 : href.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (getClass() != obj.getClass())
            return false;
        Mark other = (Mark) obj;
        if (category == null) {
            if (other.category != null)
                return false;
        } else if (!category.equals(other.category))
            return false;
        if (!StringUtils.equals(href, other.href)) {
            return false;
        }
        if (!StringUtils.equals(name, other.name)) {
            return false;
        }
        return true;
    }

}
