package com.control.back.halo.bookmark.parse;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URLEncoder;

import com.control.back.halo.bookmark.http.HttpRequest;

public final class Url2io {

    private static String rUrl = "http://api.url2io.com/article";

    public static String getArticleByUrl(String url) throws URISyntaxException, IOException {
        String params = "token={token}&url={url}";
        params = params.replace("{token}", "Ml4YbykOTPuvZUSSAurz3g").replace("{url}", URLEncoder.encode(url, "utf-8"));
        String content = HttpRequest.sendGet(rUrl, params);
        return content;
    }

    class Article {

        String url;
        String date;
        String content;
        String title;
    }
}
