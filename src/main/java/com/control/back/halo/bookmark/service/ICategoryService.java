package com.control.back.halo.bookmark.service;

import java.util.List;
import java.util.Map;

import com.control.back.halo.basic.entity.vo.TreeNode;
import com.control.back.halo.basic.service.IBaseService;
import com.control.back.halo.bookmark.entity.Category;

public interface ICategoryService extends IBaseService<Category, Long> {


    List<TreeNode> loadCategoryTree();

    /**
     * 
     * 功能描述: <br>
     * 查询重复的分类信息
     *
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public Map<String, List<Category>> findRepeatCategory();

    /**
     * 
     * 功能描述: <br>
     * 合并分类 ，首先查询其他分类下时候有子分类和书签，如果存在，就合并到主分类上 之后在删除分类
     *
     * @param mId 主分类id
     * @param cIds 其他分类ids
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public boolean mergeCategory(Long mId, String cIds);

    /**
     * 
     * 功能描述: <br>
     * 根绝ids 查询分类信息
     *
     * @param ids
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public List<Category> findCategoryByIds(String ids);
    
    public List<Category> findCategoryByName(String name);

}
