package com.control.back.halo.bookmark.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.control.back.halo.basic.service.IBaseService;
import com.control.back.halo.bookmark.entity.Mark;

public interface IMarkService extends IBaseService<Mark, Long> {

    /**
     * 
     * 功能描述: <br>
     * 分析浏览器标签HTML文件
     *
     * @param is 输入流
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public boolean analysisBrowerMarkHtmlAndSave(InputStream is);

    /**
     * 
     * 功能描述: <br>
     * 查询重复的书签
     *
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public Map<String, List<Mark>> findRepeatMark();

    public void moveMarkToCategory(Long mId, Long categoryId);

    public Page<Mark> findMarksBy(String categoryIds, String name, Pageable pageable);

}
