package com.control.back.halo.manage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.control.back.halo.basic.controller.BaseController;
import com.control.back.halo.manage.entity.Admin;
import com.control.back.halo.manage.service.IAdminService;
import com.control.back.halo.manage.service.IRoleService;

@Controller
@RequestMapping("/admin")
public class AdminController extends BaseController {

    @Autowired
    private IAdminService adminService;

    @Autowired
    private IRoleService  roleService;

    @RequestMapping({ "", "index" })
    public String index(Model model) {
        model.addAttribute("admins", adminService.findAll());
        return "admin/index";
    }

    @RequestMapping("/remove")
    public String remove(Long id) {
        adminService.delete(id);
        return "redirect:index.html";
    }

    @RequestMapping("/oauth")
    public String oauth(Long id, Model model) {
        model.addAttribute("admin", adminService.find(id));
        model.addAttribute("roles", roleService.findAll());
        return "authorization/userIndex";
    }

    @RequestMapping("/oauthsubmit")
    public String oauthsubmit(Long id, Long[] roleIds) {
        adminService.saveAdminRoles(id, roleIds);
        return "redirect:index.html";
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(Admin admin) {
        adminService.saveOrUpdate(admin);
        return "redirect:index.html";
    }
}
