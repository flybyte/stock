package com.control.back.halo.manage.service;

import java.util.List;

import com.control.back.halo.basic.service.IBaseService;
import com.control.back.halo.manage.entity.Admin;
import com.control.back.halo.manage.entity.Function;

public interface IAdminService extends IBaseService<Admin, Long> {

    Admin findByUsername(String username);

    void saveOrUpdate(Admin admin);

    Boolean saveAdminRoles(Long id, Long[] roles);

    public List<Function> loadCurrentUserFunctions();

}
