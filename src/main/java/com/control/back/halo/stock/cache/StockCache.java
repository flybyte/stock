package com.control.back.halo.stock.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.control.back.halo.basic.entity.BaseEntity;

public abstract class StockCache<V extends BaseEntity> {

    Map<String, V> cache;

    String         cacheName;

    public Map<String, V> getCache() {
        return cache;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
        initCache();
    }

    public void initCache() {
        if (cache == null) {
            cache = new HashMap<>();
        }
    }

    public List<V> queryAll() {
        List<V> reinfos = new ArrayList<V>();
        reinfos.addAll(cache.values());
        return reinfos;
    }

    private String getKey(String key) {
        return cacheName + "_" + key;
    }

    private String getKey(Long key) {
        return cacheName + "_" + key;
    }

    public void put(String key, V value) {
        cache.put(getKey(key), value);
    }

    public V get(String key) {
        return cache.get(getKey(key));
    }

    public void insert(V v) {
        if (v == null || v.getId() == null) { return; }
        cache.put(getKey(v.getId()), v);
    }

    public void insert(List<V> rs) {
        for (V v : rs) {
            insert(v);
        }
    }

    public void update(V r) {
        if (r == null || r.getId() == null) { return; }
        cache.put(getKey(r.getId()), r);
    }

    public void remove(V r) {
        if (r == null) { return; }
        remove(r.getId() + "");
    }

    public void remove(String k) {
        if (k == null) { return; }
        cache.remove(getKey(k));
    }

    public void removeAll() {
        cache.clear();
    }
}