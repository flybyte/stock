package com.control.back.halo.stock.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "stock")
public class StockProperties {

    private String companyStockCollection   = "company_stock";

    private String stabilitystockCollection = "near_{0}_stabilitystock";

    private String yahooStockUrlDay         = "http://ichart.yahoo.com/table.csv?s={0}&a={1}&b={2}&c={3}&d={4}&e={5}&f={6}&g=d";

    private String yahooStockRtRequestUrl   = "http://finance.yahoo.com/d/quotes.csv?s={0}&f={1}";

    private String sinaStockRtRequestUrl    = "http://hq.sinajs.cn/list={0}";

    private String sinaStockUrlDay          = "http://money.finance.sina.com.cn/corp/go.php/vMS_MarketHistory/stockid/{0}.phtml?year={1}&jidu={2}";

    private String companyStockSynUrl       = "http://quote.eastmoney.com/stocklist.html";

    private String companyStockSynDivId     = "quotesearch";

    private String allStockFileAddress      = "E:\\halo_index\\hzstock\\allstock.txt";

    private String buyStockFileAddress      = "E:\\halo_index\\hzstock\\buystock.txt";

    public String getCompanyStockCollection() {
        return companyStockCollection;
    }

    public void setCompanyStockCollection(String companyStockCollection) {
        this.companyStockCollection = companyStockCollection;
    }

    public String getStabilitystockCollection() {
        return stabilitystockCollection;
    }

    public void setStabilitystockCollection(String stabilitystockCollection) {
        this.stabilitystockCollection = stabilitystockCollection;
    }

    public String getYahooStockUrlDay() {
        return yahooStockUrlDay;
    }

    public void setYahooStockUrlDay(String yahooStockUrlDay) {
        this.yahooStockUrlDay = yahooStockUrlDay;
    }

    public String getYahooStockRtRequestUrl() {
        return yahooStockRtRequestUrl;
    }

    public void setYahooStockRtRequestUrl(String yahooStockRtRequestUrl) {
        this.yahooStockRtRequestUrl = yahooStockRtRequestUrl;
    }

    public String getSinaStockRtRequestUrl() {
        return sinaStockRtRequestUrl;
    }

    public void setSinaStockRtRequestUrl(String sinaStockRtRequestUrl) {
        this.sinaStockRtRequestUrl = sinaStockRtRequestUrl;
    }

    public String getSinaStockUrlDay() {
        return sinaStockUrlDay;
    }

    public void setSinaStockUrlDay(String sinaStockUrlDay) {
        this.sinaStockUrlDay = sinaStockUrlDay;
    }

    public String getCompanyStockSynUrl() {
        return companyStockSynUrl;
    }

    public void setCompanyStockSynUrl(String companyStockSynUrl) {
        this.companyStockSynUrl = companyStockSynUrl;
    }

    public String getCompanyStockSynDivId() {
        return companyStockSynDivId;
    }

    public void setCompanyStockSynDivId(String companyStockSynDivId) {
        this.companyStockSynDivId = companyStockSynDivId;
    }

    public String getAllStockFileAddress() {
        return allStockFileAddress;
    }

    public void setAllStockFileAddress(String allStockFileAddress) {
        this.allStockFileAddress = allStockFileAddress;
    }

    public String getBuyStockFileAddress() {
        return buyStockFileAddress;
    }

    public void setBuyStockFileAddress(String buyStockFileAddress) {
        this.buyStockFileAddress = buyStockFileAddress;
    }
}