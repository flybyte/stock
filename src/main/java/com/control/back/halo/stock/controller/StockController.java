package com.control.back.halo.stock.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.control.back.halo.basic.controller.BaseController;
import com.control.back.halo.stock.data.entity.StockInfo;
import com.control.back.halo.stock.service.StockInfoService;

@Controller
@RequestMapping("/stock")
public class StockController extends BaseController {

    private StockInfoService stockInfoService = StockInfoService.getInstance();

    @RequestMapping("/index")
    public String index(Model model) {
        List<StockInfo> sinfos = stockInfoService.queryAll();
        model.addAttribute("stockInfos", sinfos);
        return "stock/stockInfo";
    }
}
