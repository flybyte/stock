package com.control.back.halo.stock.data;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.control.back.halo.stock.config.StockProperties;
import com.control.back.halo.stock.data.entity.StockInfo;
import com.control.back.halo.stock.util.StockUtil;

/**
 * 股票信息存储
 * 
 * @author zhaohuiliang
 *
 */
@EnableConfigurationProperties(StockProperties.class)
public abstract class StockInfoStorage {

    @Autowired
    StockProperties stockProperties;

    public abstract List<StockInfo> queryAll();

    public abstract long count();

    public abstract void insertStock(List<StockInfo> stcoks);

    /**
     * 初始化所有的股票信息 1.先解析所有的票，然后在对股票进行持久化
     */
    @PostConstruct
    public void stockDataPersistence() {
        if (count() <= 0) {
            List<StockInfo> compantStocks = parseStockInfo();
            insertStock(compantStocks);
        }
    };

    /**
     * 解析股票信息
     * 
     * @return
     */
    private List<StockInfo> parseStockInfo() {
        StockUtil su = new StockUtil(stockProperties.getCompanyStockSynUrl(), stockProperties.getCompanyStockSynDivId());
        return su.getCompanyStockList();
    }

}
