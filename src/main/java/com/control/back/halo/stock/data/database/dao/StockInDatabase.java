package com.control.back.halo.stock.data.database.dao;

import org.springframework.stereotype.Repository;

import com.control.back.halo.basic.dao.IBaseDao;
import com.control.back.halo.stock.data.entity.StockInfo;

@Repository
public interface StockInDatabase extends IBaseDao<StockInfo, Long> {

    public StockInfo queryByStockCode(String stockCode);

}
