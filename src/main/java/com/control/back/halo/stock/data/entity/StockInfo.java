package com.control.back.halo.stock.data.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.control.back.halo.basic.entity.BaseEntity;

@Entity
@Table(name = "s_stock_info")
public class StockInfo extends BaseEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 6497450308030484369L;

    public static String      BUY_IN_TYPE      = "buyIn";
    public static String      BUY_OUT_TYPE     = "buyOut";

    /**
     * 股票代码
     */
    String                    stockCode;

    /**
     * 股票名称
     */
    String                    stockName;

    /**
     * 当前价格
     */
    Double                    currentPrice;

    /**
     * 当前时间
     */
    Date                      nowDate;

    /**
     * 盈利金额
     */
    Double                    profitStockPrice;

    /**
     * buyIn，buyOut
     */
    String                    type;

    /**
     * 所属机构 sh or sz
     */
    private String            teamOrg;

    public StockInfo() {

    }

    public StockInfo(String stockCode, Double currentPrice, Date nowDate, Double profitStockPrice, String type) {
        super();
        this.stockCode = stockCode;
        this.currentPrice = currentPrice;
        this.nowDate = nowDate;
        this.profitStockPrice = profitStockPrice;
        this.type = type;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getTeamOrg() {
        return teamOrg;
    }

    public void setTeamOrg(String teamOrg) {
        this.teamOrg = teamOrg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getProfitStockPrice() {
        return profitStockPrice;
    }

    public void setProfitStockPrice(Double profitStockPrice) {
        this.profitStockPrice = profitStockPrice;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public Double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Date getNowDate() {
        return nowDate;
    }

    public void setNowDate(Date nowDate) {
        this.nowDate = nowDate;
    }

    @Override
    public String toString() {
        return "{stockCode:" + stockCode + ", stockName:" + stockName + ", currentPrice:" + currentPrice + ", currentDate:" + nowDate + ", profitStockPrice:" + profitStockPrice + ", type:" + type + ", teamOrg:" + teamOrg + "}";
    }

    public String toPrintString() {
        return "购买股票 [股票代码：" + stockCode + ", 股票当前价格：" + currentPrice + ",盈利金額:" + profitStockPrice + ", 购买日期:" + nowDate + ",购买标识：" + type + "]";
    }

}
