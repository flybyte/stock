package com.control.back.halo.stock.data.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.control.back.halo.stock.config.StockProperties;
import com.control.back.halo.stock.data.StockInfoStorage;
import com.control.back.halo.stock.data.entity.StockInfo;

@Service("stockFileStorage")
public class StockInFileStorage extends StockInfoStorage {

    private static Log      logger = LogFactory.getLog(StockInFileStorage.class);

    @Autowired
    private StockProperties stockProperties;

    public void insertStock(List<StockInfo> stcoks) {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(stockProperties.getAllStockFileAddress())), "utf-8"));
            for (Iterator<StockInfo> iterator = stcoks.iterator(); iterator.hasNext();) {
                Object dbObject = iterator.next();
                bw.append(dbObject.toString()).append("\r\n");
            }
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException", e);
        } catch (IOException e) {
            logger.error("IOException", e);
        } finally {
            if (bw != null) {
                try {
                    bw.flush();
                    bw.close();
                } catch (IOException e) {
                    logger.error("IOException", e);
                }
            }
        }

    }

    public List<StockInfo> queryAll() {
        List<StockInfo> cstocks = new ArrayList<StockInfo>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(stockProperties.getAllStockFileAddress())), "utf-8"));
            String str = null;
            while ((str = br.readLine()) != null) {
                try {
                    StockInfo cstock = JSON.parseObject(str, StockInfo.class);
                    cstocks.add(cstock);
                } catch (Exception e) {
                    logger.error("打印无法序列化的字符串信息：" + str, e);
                }
            }
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException", e);
        } catch (IOException e) {
            logger.error("IOException", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    logger.error("IOException", e);
                }
            }
        }
        return cstocks;
    }

    @Override
    public long count() {
        List<StockInfo> sinfos = queryAll();
        return sinfos.size();
    }
}
