package com.control.back.halo.stock.entity;

import java.io.Serializable;
import java.util.Date;

public class YaHooStockEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6916655560989258911L;

    // 记录时间
    private Date              recordDate;

    // 开盘金额
    private Double            openPrice;

    // 最高金额
    private Double            highPrice;

    // 最低金额
    private Double            lowPrice;

    // 收盘金额
    private Double            closePrice;

    // 成交金额
    private Double            volumePrice;

    // adj Close
    private Double            adjClosePrice;

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public Double getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(Double openPrice) {
        this.openPrice = openPrice;
    }

    public Double getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(Double highPrice) {
        this.highPrice = highPrice;
    }

    public Double getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(Double lowPrice) {
        this.lowPrice = lowPrice;
    }

    public Double getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(Double closePrice) {
        this.closePrice = closePrice;
    }

    public Double getVolumePrice() {
        return volumePrice;
    }

    public void setVolumePrice(Double volumePrice) {
        this.volumePrice = volumePrice;
    }

    public Double getAdjClosePrice() {
        return adjClosePrice;
    }

    public void setAdjClosePrice(Double adjClosePrice) {
        this.adjClosePrice = adjClosePrice;
    }

    @Override
    public String toString() {
        return "YaHooStockEntity [recordDate=" + recordDate + ", openPrice=" + openPrice + ", highPrice=" + highPrice + ", lowPrice=" + lowPrice + ", closePrice=" + closePrice + ", volumePrice=" + volumePrice + ", adjClosePrice=" + adjClosePrice + "]";
    }

    public YaHooStockEntity(Date recordDate, Double openPrice, Double highPrice, Double lowPrice, Double closePrice, Double volumePrice, Double adjClosePrice) {
        super();
        this.recordDate = recordDate;
        this.openPrice = openPrice;
        this.highPrice = highPrice;
        this.lowPrice = lowPrice;
        this.closePrice = closePrice;
        this.volumePrice = volumePrice;
        this.adjClosePrice = adjClosePrice;
    }

    public YaHooStockEntity() {
        super();
    }
}
