package com.control.back.halo.stock.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.control.back.halo.stock.job.StockJobRun;
import com.control.back.halo.stock.job.StockWriteFileJobRun;

public class StockBuyListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		StockJobRun.run();
		StockWriteFileJobRun.run();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

	}
}