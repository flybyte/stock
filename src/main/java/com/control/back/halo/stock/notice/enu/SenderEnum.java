package com.control.back.halo.stock.notice.enu;

public enum SenderEnum {

    SMS("sms"), MAIL("mail"), WECHAT("wechat"), QQ("qq");

    String name;

    SenderEnum(String name) {
        this.name = name;
    }
}
