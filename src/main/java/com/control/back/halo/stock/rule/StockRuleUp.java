package com.control.back.halo.stock.rule;

import java.util.Date;

import com.control.back.halo.stock.data.entity.StockInfo;
import com.control.back.halo.stock.notice.AlertMsg;
import com.control.back.halo.stock.notice.MsgNotice;

public class StockRuleUp extends StockRule {

    public StockRuleUp() {
        setMsgNotice(AlertMsg.getInstance());
    }

    public StockRuleUp(MsgNotice<String, StockInfo> msgNotice) {
        setMsgNotice(msgNotice);
    }

    @Override
    public void judgeStockTrend(String stockCode, Double currentPrice, Double profitStockPrice) {
        String maxKey = "Max" + stockCode;

        if (stockTrend.get(maxKey) == null) {
            stockTrend.put(maxKey, profitStockPrice + 50);
        }

        Double maxPrice = stockTrend.get(maxKey);

        if (logger.isDebugEnabled()) {
            logger.debug("股票代码:" + stockCode + "最大盈利金额：" + maxPrice);
        }

        StockInfo info = new StockInfo(stockCode, currentPrice, new Date(), profitStockPrice, "卖出");

        if (profitStockPrice > maxPrice) {
            stockTrend.put(maxKey, profitStockPrice + 50);
        } else if (profitStockPrice < (maxPrice - 100)) {
            stockPro.put(stockCode, info);
        }

        if (profitStockPrice < -150) {
            stockPro.put(stockCode, info);
        }
    }

}
