<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>股票</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
    <link href="/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
	<link href="//cdn.bootcss.com/iCheck/1.0.2/skins/all.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
	<link href="//cdn.bootcss.com/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet">

	<script type="text/javascript">
		function domain() {
			document.getElementById("myForm").submit()
		}
		setInterval("domain()", 30000);
		
		function showImg(stockCode){
			document.getElementById(stockCode).style.display='block';
		}
		
		function hideImg(stockCode){
			document.getElementById(stockCode).style.display='none';
		}
	</script>
</head>
<body>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>股票</h2>
        <ol class="breadcrumb">
            <li><a href="index.html">主页</a></li>
            <li><a>购买股票推荐</a></li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
	<form id="myForm" action="/stock/index.htm" method="post">
		<input text="text" 
			name="oprType" value="query" style="display:none;"><br> <input type="submit"
			value="提交" style="display:none;" />
	</form>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>股票</h5>
                </div>
                <div class="ibox-content">
                    <table
                            class="table table-striped table-bordered table-hover dataTables-example role-table">
                        <thead>
                        <tr>
							<td>序列号</td>
							<td align="center">股票代码</td>
							<td align="center">买入价格</td>
							<td align="center">购买时间</td>
							<td align="center">操盘盈利</td>
							<td align="center">操作类型</td>
							<!-- 自定义标签 -->
						</tr>
                        </thead>
                        <tbody>
                        <#list stockInfos as stock>
                        <tr>
							<td>${stock_index + 1}</td>
							<td align="center" >
								<a onMouseOut="hideImg('${stock.stockCode}')" onmouseover="showImg('${stock.stockCode}')">${stock.stockCode}</a>
								<div id="${stock.stockCode}" style="display:none;height:50px;back-ground:blue;position:absolute;">
									<img alt="sh601008" src="http://image.sinajs.cn/newchart/daily/n/${stock.stockCode}.gif" style="opacity:1.0;background-color:lightcyan;">
								</div>	
							</td>
							<td align="center">${stock.currentPrice?string('#.##')} </td>
							<td align="center">${stock.nowDate?string('yyyy-MM-dd HH:mm:ss')} </td>
							<td align="center">${stock.profitStockPrice?string('#.##')} </td>
							<td align="center">${stock.type}</td>
							<!-- 自定义标签 -->
						</tr>
                        </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>	
</body>
</html>